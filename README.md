# Live-coding Challenge
This repository contains code of intentionally dubious quality. It should serve as an entry point to discussion and practical exercise.

#### The "Architecture"
```mermaid
graph LR
S1(ProductSource) --> |fetchProducts<br>storeProduct| ProductContext
ProductContext --> |useProducts| App(&ltApp/&gt)
App --> ProductList(&ltProductList/&gt)
ProductList --> P1(&ltProductListItem/&gt)
ProductList --> P2(&ltProductListItem/&gt)
ProductList --> P3(&ltProductListItem/&gt)
App --> Bar(&ltSearchBar/&gt)
```

#### Questions / Tasks
1. What do you think about the overall structure? Does it seem to have any particular up- or downsides?
2. How to prevent the click event on ProductListItem's text area from propagating and triggering the click handler on its parent.
3. How many renders will happen in the course of `product-provider.tsx:28 - getProducts()`?
4. How to deal with the API taking pretty much _random_ time to process requests, which can result in responses being handled out of sequence?
5. How to fix the atrocious rendering performance? Hint: most performance is lost during image generation in `<PixelGrid />`. The offending method is called `generateImageDataURL` - it's synchronous (happily blocking main thread for at least 200ms) and deterministic but - for the purposes of this exercise - ** it can not be changed**.
6. How would you solve the problem of too many requests/rerenders happening whenever input/textarea values change?
7. Why does the red "Loading" button (which replaces blue "Clear search" button) transition from blue to red, when rendered? It is a separate React node, after all. By design, the transition should happen when "Clear search" button changes states between enabled and disabled. There should be no CSS transition between this button and "Loading..".
8. Feature request: "Add to favourites", but the backend is not ready. There will be a couple of endpoints for this, in the future, but for now, the feature should use memory (or local storage). The idea is to not have to change a lot of code, once the backend support arrives.
